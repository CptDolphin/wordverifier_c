﻿using System;
using System.IO;


namespace WordUnscrambler.Files
{
    public class FileWrapper : IFileWrapper
    {
        public string ReadTextFromFile(string filePath)
        {
            return File.ReadAllText(filePath);
        }
    }
}
