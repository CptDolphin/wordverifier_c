﻿using System;

namespace WordUnscrambler.Files
{
    public interface IFileWrapper
    {
        string ReadTextFromFile(string filePath);
    }
}
