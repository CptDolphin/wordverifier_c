﻿using System;
using WordUnscrambler.Workers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WordUnscrambler.Test.Unit
{
    [TestClass]
    public class WordMatcherTest
    {
        private readonly WordMatcher _wordMatcher = new WordMatcher();

        [TestMethod]
        public void ScrambledWordMatchesGivenWordInTheList()
        {
            string[] words = { "cat", "house", "more" };
            string[] scrambledWords = { "act", "orme" };

            var matchedWords = _wordMatcher.Match(scrambledWords, words);

            Assert.IsTrue(matchedWords.Count == 2);
            Assert.IsTrue(matchedWords[0].ScrambledWord.Equals("act"));
            Assert.IsTrue(matchedWords[0].Word.Equals("cat"));
            Assert.IsTrue(matchedWords[1].ScrambledWord.Equals("orme"));
            Assert.IsTrue(matchedWords[1].Word.Equals("more"));
        }
    }
}
