﻿using System;
using System.IO;
using System.IO.Abstractions;
using WordUnscrambler.Workers;
using WordUnscrambler.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace WordUnscrambler.Test.Unit
{
    [TestClass]
    public class FileReaderTest
    {

        private const string TEST_FILE_NAME = "Not_exisiting_file.txt";

        private readonly FileReader _fileReader = new FileReader();

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void FileReaderReaderException()
        {
            _fileReader.Read(TEST_FILE_NAME);
        }


    }
}
